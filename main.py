import pygame as pg
import pygame.locals as pgl
import OpenGL.GL as gl
import OpenGL.GLU as glu

from main_consts import *
import _shapes as sh

# contains objects having get_vertice() and get_edges() methods.
GL_CONTAINER = dict()

def main():
    init_pygame()
    init_gl()
    init_gl_container()
    init_mainloop()

def init_pygame():
    pg.init()
    pg.display.set_mode(DISPLAY, pgl.DOUBLEBUF|pgl.OPENGL)
    print("init_pygame OK")

def init_gl():
    # TODO: move to main_consts.py
    glu.gluPerspective(45, DISPLAY_RATIO, 0.1, 50)
    gl.glTranslatef(0,0,-5)
    gl.glRotate(0,0,0,0)
    gl.glClear(gl.GL_COLOR_BUFFER_BIT|gl.GL_DEPTH_BUFFER_BIT)
    print("init_gl OK")

def init_gl_container():
    GL_CONTAINER["cube_0"] = sh.init_shape

def init_mainloop():
    print("starting main loop")
    while(True):
        # labas
        handle_events()
        print("qwe")
        set_perspective()
        draw()
        pg.time.wait(5)

def handle_events():
    for event in pg.event.get():
        if event.type == pg.QUIT:
            print("stoping main loop")
            pg.quit() # move to another place
            quit() # or return??

def set_perspective():
    gl.glRotate(1,2,3,4)

def draw():
    gl.glClear(gl.GL_COLOR_BUFFER_BIT|gl.GL_DEPTH_BUFFER_BIT)
    # draw edges:
    gl.glBegin(gl.GL_LINES)
    for gl_object in GL_CONTAINER.values():
        vertices = gl_object.get_vertices()
        edges = gl_object.get_edges()
        for edge in edges:
            for vertex_id in edge:
                gl.glVertex3fv(vertices[vertex_id])

    gl.glEnd()
    pg.display.flip()

if __name__ == "__main__":
    main()
