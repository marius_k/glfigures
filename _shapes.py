
import math

math.tau = 2 * math.pi

class Cube:
    _vertices = (
        (1,1,1),
        (1,1,-1),
        (1,-1,1),
        (1,-1,-1),
        (-1,1,1),
        (-1,1,-1),
        (-1,-1,1),
        (-1,-1,-1),
    )

    _edges = (
        (0,1),
        (2,3),
        (0,2),
        (1,3),

        (4,5),
        (6,7),
        (4,6),
        (5,7),

        (0,4),
        (1,5),
        (2,6),
        (3,7),
    )

    def get_vertices(self):
        return Cube._vertices

    def get_edges(self):
        return Cube._edges




class Shape:

    def __init__(self, configuration):
        #self._configuration = configuration # dict n-kampis -> kiekis

        for key in configuration:
            p_vertices = Shape.get_plane_vertices(key)
            p_edges = dict()
            for v_id in p_vertices:
                p_edges[v_id] = (v_id, (v_id+1)%key)

            self._vertices = tuple(p_vertices.values())
            self._edges = tuple(p_edges.values())
            break


    def get_plane_vertices(n):
        vertices = dict()
        for i in range(n):
            c_point = math.e ** ( math.tau * 1j * (i/n))
            x = c_point.real
            y = c_point.imag
            vertices[i] = (x, y, 0)
        return vertices


    def get_vertices(self):
        return self._vertices



    def get_edges(self):
        return self._edges



configuration = dict()
configuration[5] = 3



init_shape = Shape(configuration)
